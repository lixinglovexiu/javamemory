package string;

public class StringEquals {
    private static void judgeEqual() {
        /**
         * abcd建立在常量池中，str1,str2使用的都是常量池中abcd对象的引用
         * str1 == str2
         */
        String str1 = "abcd";
        String str2 = "abcd";
        System.out.println(str1 == str2);

        /*
         * 下面的操作编译后的执行过程是
         * str3 = new StringBuffer(str3);
         * str3.append("d")
         * 所以str3 != str1
         */
        String str3 = "abc";
        str3 += "d";
        System.out.println(str1 == str3);

        /*
         * new的对象是存在堆里的，str4指向的是堆中的一个对象，而非常量池中的对象
         * str1 != str4
         */
        String str4 = new String("abcd");
        System.out.println(str1 == str4);

        /*
         * 字符串为常量，编译时直接存储值而非引用
         * abc，d都不会创建对象，最后只会将得到的结果abcd的常量池引用赋给str5
         * str1 == str5
         */
        String str5 = "abc" + "d";
        System.out.println(str1 == str5);

    }

    public static void main(String[] args) {
        judgeEqual();
    }
}